<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<title>Unified Patents - PTAB</title>
<meta charset="utf-8">

<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.css">
<link rel="stylesheet" href='<c:url value="/resources/flat-ui/css/flat-ui.css" />'>
<link rel="stylesheet" href='<c:url value="/resources/flat-ui/css/bootstrap-switch.css" />'>
<link rel="stylesheet"
	href='<c:url value="/resources/backgrid/css/backgrid-paginator.css" />'>
<link rel="stylesheet" href='./resources/backgrid/css/backgrid.css'>
<link rel="stylesheet" href='./resources/backgrid/css/backgrid-text-cell.css'>
<link rel="stylesheet" href='./resources/backgrid/css/backgrid-filter.css'>
<link rel="stylesheet" href='./resources/backgrid/css/backgrid-select-all.css'>
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href='./resources/css/application.css'>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script
	src="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src='<c:url value="/resources/backgrid/js/bootstrap-modal.js" />'></script>
<script src='<c:url value="/resources/flat-ui/js/flatui-checkbox.js" />'></script>
<script src='<c:url value="/resources/flat-ui/js/bootstrap-switch-3.0.2.js" />'></script>
<script src='<c:url value="/resources/flat-ui/js/flatui-radio.js" />'></script>
<script src='<c:url value="/resources/flat-ui/js/bootstrap-select.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/underscore-1.6.0.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backbone1.1.2.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backbone.paginator.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid-paginator.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid-text-cell.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid-select-all.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid-filter.js" />'></script>
<script src='<c:url value="/resources/plugins/jquery-file-download/js/jquery.filedownload.js" />'></script>
<script src='<c:url value="/resources/plugins/jquery.dateFormat/js/dateFormat.js" />'></script>
<script src='<c:url value="/resources/plugins/jquery.dateFormat/js/jquery.dateFormat.js" />'></script>
<script src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="http://www.amcharts.com/lib/3/pie.js"></script>
<script src="http://www.amcharts.com/lib/3/themes/light.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#brandTab').click(function(){
			window.location.href = '<c:url value="/index.jsp"/>';
		});	
		$("input[grid-col-name]").bootstrapSwitch();
		$('#settings').click(function(event){
			event.preventDefault();
			$('#settingModal').modal();
			return false;
		});
		$('input[grid-col-name]').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
			console.log(this); // DOM element
			console.log(event); // jQuery event
			console.log(state); // true | false
			
			if(!state)
				caseGrid.removeColumn(caseGrid.columns.where({name : $(this).attr('grid-col-name')}));
			else{
				console.log(caseGridColumns.where({name : $(this).attr('grid-col-name')}));
				console.log($(this).attr('col-index'));
				var modelToInsert = caseGridColumns.where({name : $(this).attr('grid-col-name')});
				caseGrid.insertColumn(modelToInsert, {at : $(this).attr('col-index')});
			}
		});
	});
	var caseNumCell = Backgrid.Cell.extend({
		render : function() {
			this.$el.html('<a href="#" onclick=openCaseDetails(\'' + this.model.get('caseId') + '\',\'' + this.model.get('patentNumber') + '\')>' + this.model.get('caseId') + '</a>');
			this.delegateEvents();
			return this;
		}
	});
	
	var searchableCell = Backgrid.Cell.extend({
		render : function() {
			this.$el.html('<a href="#" onclick="filterCases(\'' + this.model.get(this.column.get('name')) + '\',\'' + this.column.get('name') + '\')">' + this.model.get(this.column.get('name')) + '</a>');
			this.delegateEvents();
			return this;
		}
	});	
	var searchableDateCell = Backgrid.DateCell.extend({
		render : function() {
			this.$el.html('<a href="#" onclick="filterCases(\'' + this.formatter.fromRaw(this.model.get(this.column.get("name")), this.model) + '\',\'' + this.column.get('name') + '\')">' + this.formatter.fromRaw(this.model.get(this.column.get("name")), this.model) + '</a>');
			this.delegateEvents();
			return this;
		}
	});	
	var docNameCell = Backgrid.Cell.extend({
		render : function() {
			this.$el.html('<a href="#" onclick=downloadFile(' + this.model.get("id") + ')>' + this.model.get('name') + '</a>');
			this.delegateEvents();
			return this;
		}
	});

	// Below cell editor is un-used yet an importan working example
	var CategorySelectCellEditor = Backgrid.SelectCellEditor.extend({
		save : function() {
			var model = this.model;
			var column = this.column;
			model.set(column.get("name"), this.formatter.toRaw(this.$el.val(), model));
			alert('I am called!!');
		}
	});
	function openCaseDetails(case_id, patentNumber){
		window.open("http://www.google.com/patents/US" + patentNumber, "_blank");
		window.location.href = '<c:url value="/rest/patents/cases/"/>'+case_id;
	}
</script>
<script src='<c:url value="/resources/js/caseColumns.js" />'></script>
<script src='<c:url value="/resources/js/custom.js" />'></script>
</head>
<body>
	<div class="container" style="overflow: hidden; width: auto;">
		<jsp:include page="jsps/header.jsp"/>
		<jsp:include page="jsps/top-nav.jsp"/>
		<div id="preparing-file-modal" title="Preparing report..."
			style="display: none;">
			Your download is being prepared. Please wait...
	
			<div class="ui-progressbar-value ui-corner-left ui-corner-right"
				style="width: 100%; height: 22px; margin-top: 20px;"></div>
		</div>

		<div id="error-modal" title="Error" style="display: none;">There
			was a problem downloading the document, please try again.
		</div>		  
		<div class="row" id="containerDiv">
			<div class="col-md-2" id="left-pane">
				<jsp:include page="jsps/left-nav.jsp"/>
			</div>
			<div class="col-md-10" id="right-pane">
				<div id="resetForQueryDiv" style="display:none;">
					<a id="queryReset" href="#fakelink" class="btn btn-block btn-lg btn-info">Reset Query Result</a>
				</div>
				<jsp:include page="jsps/range-filter.jsp"/>
				<jsp:include page="jsps/content-cases.jsp"/>
				<jsp:include page="jsps/content-chart.jsp"/>
			</div>
		</div>
	</div>
	<form action="<c:url value="/rest/patents/settings" />" method="POST">
		<div class="modal fade" id="settingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span><span class="sr-only">Close</span></button>
			        <h4 class="modal-title" id="myModalLabel">Configure Column Display</h4>
			      </div>
			      <div class="modal-body">
			      	<table>
			      		<tbody>
							<tr><td style="padding-right: 10px;">Case Number</td><td><input name="caseNumSwitch" type="checkbox" col-index="0" checked data-toggle="switch" grid-col-name="caseId"/></td></tr>
							<tr><td style="padding-right: 10px;">Application Number</td><td><input name="applicationNumSwitch" type="checkbox" col-index="1" checked data-toggle="switch"  grid-col-name="applicationNumber"/></td></tr>
							<tr><td style="padding-right: 10px;">Filling date</td><td><input name="fillingDateSwitch" type="checkbox" col-index="2" checked data-toggle="switch"  grid-col-name="fillingDate"/></td></tr>
							<tr><td style="padding-right: 10px;">Patent Number</td><td><input name="patentNumSwitch" type="checkbox" col-index="3" checked data-toggle="switch"  grid-col-name="patentNumber"/></td></tr>
							<tr><td style="padding-right: 10px;">Decision Date</td><td><input name="decisionDateSwitch" type="checkbox" col-index="4" checked data-toggle="switch"  grid-col-name="decisionDate"/></td></tr>
							<tr><td style="padding-right: 10px;">Petitioner</td><td><input name="petitionerSwitch" type="checkbox" col-index="5" checked data-toggle="switch"  grid-col-name="petitioner"/></td></tr>
							<tr><td style="padding-right: 10px;">Patent Owner</td><td><input name="patentOwnerSwitch" type="checkbox" col-index="6" checked data-toggle="switch"  grid-col-name="patentOwner"/></td></tr>
							<tr><td style="padding-right: 10px;">Status</td><td><input name="statusSwitch" type="checkbox" col-index="7" checked data-toggle="switch"  grid-col-name="status"/></td></tr>
							<tr><td style="padding-right: 10px;">Tech Center</td><td><input name="techCenterSwitch" type="checkbox" col-index="8" checked data-toggle="switch"  grid-col-name="techCenter"/></td></tr>
							<tr><td style="padding-right: 10px;">Category</td><td><input name="categorySwitch" type="checkbox" col-index="9" checked data-toggle="switch"  grid-col-name="category"/></td></tr>
							<tr><td style="padding-right: 10px;">NPE Link</td><td><input name="npeLinkSwitch" type="checkbox" col-index="10" checked data-toggle="switch"  grid-col-name="npeLink"/></td></tr>
			      		</tbody>
			      	</table>
			      </div>
			      <div class="modal-footer">
			      	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>	
		</div>
	</form>
	<div class="modal fade" id="logModal" tabindex="-1" role="dialog" aria-labelledby="Sync Status" aria-hidden="true">
		<div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Sync Status by Case Number</h4>
		      </div>
		      <div class="modal-body" style="overflow: auto;">
		      	<table id="syncStatustable" class="table table-bordered">
		      		<thead>
		      			<tr>
		      				<td>Case Number</td>
		      				<td>Sync State</td>
		      			</tr>
		      		</thead>
		      		<tbody>
		      		
		      		</tbody>
		      	</table>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
	</div>
</body>
</html>
