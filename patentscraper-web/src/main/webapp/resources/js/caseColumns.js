var selectColumn = new Backgrid.Column({
    // name is a required parameter, but you don't really want one on a select all column
    name: "",

    // Backgrid.Extension.SelectRowCell lets you select individual rows
    cell: "select-row",

    // Backgrid.Extension.SelectAllHeaderCell lets you select all the row on a page
    headerCell: "select-all",
})
var columnCaseId = new Backgrid.Column({
	name : "id",
	cell : Backgrid.IntegerCell.extend({
		orderSeparator : ''
	}),
	sortable : false,
	editable : false,
	renderable : false
});
var columnCaseNumber = new Backgrid.Column({
	name : "caseId",
	label : "Case #",
	cell : caseNumCell,
	editable : false,
	renderable : true
});
var columnApplicationNumber = new Backgrid.Column({
	name : "applicationNumber",
	label : "App #",
	cell : "string",
	editable : false,
	renderable : true
});
var columnFillingDate = new Backgrid.Column({
	name : "fillingDate",
	label : "Filing",
	cell : searchableDateCell,
	editable : false,
	renderable : true
});
var columnPatentNumber = new Backgrid.Column({
	name : "patentNumber",
	label : "Patent #",
	cell : "string",
	editable : false,
	renderable : true
});
var columnDecisionDate = new Backgrid.Column({
	name : "decisionDate",
	label : "Latest Decision",
	cell : searchableDateCell,
	editable : false,
	renderable : true
});
var columnPetitioner = new Backgrid.Column({
	name : "petitioner",
	label : "Petitioner",
	cell : searchableCell,
	editable : false,
	renderable : true
});
var columnPatentOwner = new Backgrid.Column({
	name : "patentOwner",
	label : "Patent Owner",
	cell : searchableCell,
	editable : false,
	renderable : true
});
var columnStatus = new Backgrid.Column({
	name : "status",
	label : "Status",
	cell : searchableCell,
	editable : false,
	renderable : true
});
var columnTechCenter = new Backgrid.Column({
	name : "techCenter",
	label : "PTO Tech",
	cell : searchableCell,
	editable : false,
	renderable : true
});
var columnCategory = new Backgrid.Column({
	name : "category",
	label : "Zone",
	cell : Backgrid.SelectCell.extend({
		// It's possible to render an option group or use a
		// function to provide option values too.
		placeholder : "Choose Category",
		optionValues : [ [ "Cloud", "CLOUD" ], [ "Content", "CONTENT" ], [ "Commerce", "COMMERCE" ], [ "None", "None" ], ["", ""] ]
	}),
	sortable : false,
	editable : true,
	renderable : true
});
var columnCategory_changed = new Backgrid.Column({
	name : "category",
	label : "Zone",
	cell : "string",
	sortable : true,
	editable : false,
	renderable : true
});
var columnNPE = new Backgrid.Column({
	name : "npe",
	label : "NPE",
	cell : Backgrid.SelectCell.extend({
		optionValues : [ [ "Yes", "YES" ], [ "No", "NO" ] ]
	}),
	sortable : true,
	editable : true,
	renderable : true
});
var columnLitigationLink = new Backgrid.Column({
	name : "litigationLink",
	label : "Litigation",
	cell : Backgrid.UriCell.extend({placeholder : "Paste NPE Link"}),
	sortable : false,
	editable : true,
	renderable : true
});