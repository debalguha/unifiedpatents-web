var Document = Backbone.Model.extend({});
var History = Backbone.Model.extend({});
var currentCaseId = undefined;

var Case = Backbone.Model.extend({
	initialize : function() {
		this.set({updated : false});
		this.set({synced : true});
		//this.on('backgrid:selected', this.onSelected, this);
		this.on('backgrid:edited', this.onChange, this);
	},
	/*onSelected : function(model, value){
		if(value){
			model.set({updated : true});
			model.set({synced : false});
		}else{
			model.set({updated : false});
			model.set({synced : true});
		}
	},*/
	onChange : function(model, value) {
		this.set({updated : true});
		this.set({synced : false});
	},
	sync : function() {
		var theModel = this;
		console.log(theModel.toJSON());
		$.ajax({
			url : 'rest/patents/case/update/new',
			type : "POST",
			data : JSON.stringify(theModel),
			contentType: "application/json; charset=UTF-8",
			processData: false,
			success : function(data){
				theModel.set({synced : true});
				theModel.set({updated : false});
				if(data == 'SUCCESS')
					$('#syncStatustable tbody').append('<tr class="success"><td>'+theModel.get('caseId')+'</td><td>Suceeded</td></tr>');
				if(data == 'UNCHANGED')
					$('#syncStatustable tbody').append('<tr class="warning"><td>'+theModel.get('caseId')+'</td><td>Unchanged</td></tr>');
			},
			error : function(){
				$('#syncStatustable tbody').append('<tr class="danger"><td>'+theModel.get('caseId')+'</td><td>Failed</td></tr>');
			}
		});
	}
});

var CaseCollectionBackup = new Backbone.Collection({});
var CaseCollection = Backbone.PageableCollection.extend({
	initialize : function() {
		this.on('doRangefilter', this.doRangefilter, this),
		this.on('doInstitutedFilter', this.findInstitutedIPRsInLast30Days, this),
		this.on('doTerminatedFilter', this.findTerminatedIPRsWithoutDecisionInLast30Days, this);
		this.on('doCategoryFilter', this.doCategoryFilter, this);
		this.on('doNPEFilter', this.doNPEFilter, this);
		
		this.on('doFillingDateFilter', this.doFillingDateFilter, this);
		this.on('doDecisionDateFilter', this.doDecisionDateFilter, this);
		this.on('doTechCenterFilter', this.doTechCenterFilter, this);
		this.on('doPetitionerFilter', this.doPetitionerFilter, this);
		this.on('doPatentOwnerFilter', this.doPatentOwnerFilter, this);
		this.on('doStatusFilter', this.doStatusFilter, this);
	}, 
	state : {pageSize: 15},
	model : Case,
	url : 'rest/patents/cases',
	mode : "client",
	doFillingDateFilter : function(fillingDate) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/fillingDate/' + encodeURIComponent(fillingDate);
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
		// alert('I am called!!');
	},		
	doDecisionDateFilter : function(decisionDate) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/decisionDate/' + encodeURIComponent(decisionDate);
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
		// alert('I am called!!');
	},		
	doTechCenterFilter : function(techCenter) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/techCenter/' + encodeURIComponent(techCenter);
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
		// alert('I am called!!');
	},	
	doPetitionerFilter : function(petitioner) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/petitioner/' + encodeURIComponent(petitioner)+'/';
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
		// alert('I am called!!');
	},	
	doPatentOwnerFilter : function(patentOwner) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/patentOwner/' + encodeURIComponent(patentOwner)+'/';
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
		// alert('I am called!!');
	},	
	doStatusFilter : function(status) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/status/' + encodeURIComponent(status);
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
		// alert('I am called!!');
	},
	doRangefilter : function(startDate, endDate, filterOn) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases?from=' + startDate + '&to=' + endDate + '&type=' + filterOn;
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
		// alert('I am called!!');
	},
	findInstitutedIPRsInLast30Days : function(){
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/report/instituted';
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
	},
	findTerminatedIPRsWithoutDecisionInLast30Days : function(){
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/report/terminated';
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
	},
	doCategoryFilter: function(category) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/category/'+category;
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
	},
	doNPEFilter: function(npe) {
		var tempUrl = this.url;
		this.url = 'rest/patents/cases/npe/'+npe;
		this.fetch({
			reset : true
		});
		this.url = tempUrl;
	}	
});
var cases = new CaseCollection({});
var documentGridColumns = [ {
	name : "id",
	cell : Backgrid.IntegerCell.extend({
		orderSeparator : ''
	}),
	sortable : false,
	editable : false
}, {
	name : "name",
	label : "name",
	cell : docNameCell,
	editable : false
}, {
	name : "type",
	label : "Type",
	cell : "string",
	sortable : false,
	editable : false
}, {
	name : "fillingDate",
	label : "Filling date",
	cell : "date",
	editable : false
}, {
	name : "paperNumber",
	label : "Paper Number",
	cell : "string",
	editable : false
}, {
	name : "fillingParty",
	label : "Filling Party",
	cell : "string",
	editable : false
}, {
	name : "availability",
	label : "Availability",
	cell : "string",
	sortable : false,
	editable : false
} ];
var FocusableRow = Backgrid.Row.extend({
	  highlightColor: "lightYellow",
	  events: {
	    focusin: "rowFocused",
	    focusout: "rowLostFocus"
	  },
	  rowFocused: function() {
	    this.el.style.backgroundColor = this.highlightColor;
	  },
	  rowLostFocus: function() {
	    this.el.style.backgroundColor='transparent';
	  }
});
var caseGridColumns = new Backbone.Collection([selectColumn, columnCaseId, columnCaseNumber, columnApplicationNumber, columnFillingDate, columnPatentNumber, columnDecisionDate, columnPetitioner, columnPatentOwner, columnStatus, columnTechCenter, columnCategory, columnNPE, columnLitigationLink]);
var caseGrid = new Backgrid.Grid({
	row: FocusableRow,
	columns : caseGridColumns.clone(),
	collection : cases,
	emptyText: "No Data"
});

var casePaginator = new Backgrid.Extension.Paginator({
	collection : cases
});

var caseFilterCaseNumber = new Backgrid.Extension.ClientSideFilter({
	collection : cases,
	// The model fields to search for matches
	fields : [ 'caseId' ],
	tagName : 'div',
	className : 'input-group',
	template : _.template(
			'<input class="form-control" type="search" placeholder="Case Numbers"><span class="input-group-btn"> <button type="button" class="btn"> <span class="fui-search"></span> </span>', null, {
				variable : null
			}),
	// How long to wait after typing has stopped before searching can start
	wait : 150
});
var caseFilterApplicationNumber = new Backgrid.Extension.ClientSideFilter({
	collection : cases,
	// The model fields to search for matches
	fields : [ 'applicationNumber' ],
	tagName : 'div',
	className : 'input-group',
	template : _.template(
			'<input class="form-control" type="search" placeholder="Application Numbers"><span class="input-group-btn"> <button type="button" class="btn"> <span class="fui-search"></span> </span>',
			null, {
				variable : null
			}),
	// How long to wait after typing has stopped before searching can start
	wait : 150
});
var caseFilterPatentNumber = new Backgrid.Extension.ClientSideFilter({
	collection : cases,
	// The model fields to search for matches
	fields : [ 'patentNumber' ],
	tagName : 'div',
	className : 'input-group',
	template : _.template(
			'<input class="form-control" type="search" placeholder="Patent Numbers"><span class="input-group-btn"> <button type="button" class="btn"> <span class="fui-search"></span> </span>', null,
			{
				variable : null
			}),
	// How long to wait after typing has stopped before searching can start
	wait : 150
});
var caseFilterPatentOwner = new Backgrid.Extension.ClientSideFilter({
	collection : cases,
	// The model fields to search for matches
	fields : [ 'patentOwner' ],
	tagName : 'div',
	className : 'input-group',
	template : _.template(
			'<input class="form-control" type="search" placeholder="Patent Owner"><span class="input-group-btn"> <button type="button" class="btn"> <span class="fui-search"></span> </span>', null, {
				variable : null
			}),
	// How long to wait after typing has stopped before searching can start
	wait : 150
});
var caseFilterPetitioner = new Backgrid.Extension.ClientSideFilter({
	collection : cases,
	// The model fields to search for matches
	fields : [ 'petitioner' ],
	tagName : 'div',
	className : 'input-group',
	template : _.template(
			'<input class="form-control" type="search" placeholder="Petitioner"><span class="input-group-btn"> <button type="button" class="btn"> <span class="fui-search"></span> </span>', null, {
				variable : null
			}),
	// How long to wait after typing has stopped before searching can start
	wait : 150
});
/*
 * var caseFilterCaseNumber = new Backgrid.Extension.LunrFilter({ collection:
 * cases, // The model fields to search for matches fields: ['caseId'], tagName:
 * 'div', className: 'input-group', template: _.template('<input
 * class="form-control" type="search" placeholder="Case Numbers"><a
 * class="clear" data-backgrid-action="clear" href="#">&times;</a><span
 * class="input-group-btn"> <button type="button" class="btn"> <span
 * class="fui-search"></span> </span>', null, {variable: null}), // How long to
 * wait after typing has stopped before searching can start wait: 150 });
 */
resetCasesCollection = function() {
	cases.fetch({
		reset : true
	});
}
rePaintCasesGrid = function() {
	$("#caseGrid").empty();
	$("#casePaginator").empty();
	$("#caseGrid").append(caseGrid.render().$el);
	$("#casePaginator").append(casePaginator.render().$el);
}
$(document).ready(function() {
	resetCasesCollection();
	$('#caseNameFilterWrapper').append(caseFilterCaseNumber.render().$el);
	$('#applicationNumbereFilterWrapper').append(caseFilterApplicationNumber.render().$el);
	$('#patentNumberFilterWrapper').append(caseFilterPatentNumber.render().$el);
	$('#petitionerFilterWrapper').append(caseFilterPetitioner.render().$el);
	$('#patentOwnerFilterWrapper').append(caseFilterPatentOwner.render().$el);
	rePaintCasesGrid();
	$('#sync-action').click(function() {
		$('#logModal').modal();
		cases.forEach(function(model) {
			if (model.get("updated")) {
				model.sync();
			}
		});
	});
	$("#caseMenu").click(function() {
		$("#docGridDiv").fadeOut(1000);
		$("#hiddenModelView").fadeOut(1000);
		$("#caseGridDiv").fadeIn();
		$('#chartView').fadeOut(1000);
		$("#docMenu").removeClass("active");
		$("#caseMenu").addClass("active");
		$("#historyMenu").removeClass("active");
		$("#docMenu").fadeOut(1000);
		currentCaseId = undefined;
	});
	$("#caseNumSearchBtn").click(function() {
		var val = $("#caseNumSearch").val();
		cases.filter({
			caseId : val
		});
	});
	$('#rangeFilter').click(function() {
		filterOnRange();
	});
	$('#clearFilter').click(function() {
		resetRangeFilter();
	});	
	$("#dateFrom").datepicker({
		onClose : function(dateText, datePickerInstance){
			if(dateText == '')
				$("#startDateFormGrp").addClass('has-error');
			else
				$("#startDateFormGrp").addClass('has-success');
		}
	});
	$("#dateTo").datepicker({
		onClose : function(dateText, datePickerInstance){
			if(dateText == '')
				$("#endDateFormGrp").addClass('has-error');
			else
				$("#endDateFormGrp").addClass('has-success');
		}
	});
	$('#reset-action').click(function(){
		window.location.reload(true);
	});	
	/*$('#queryReset').click(function(){
		resetCasesCollection();
		$('#resetForQueryDiv').fadeOut();
	});*/
	$('#categorySelect').selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});
	$('#npeSelect').selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});
	$('#categorySelect').change(function(){
		/*var selectedModels = caseGrid.getSelectedModels();
		$.each(selectedModels, function(indesx, model){
			model.set('category', $('#categorySelect').val());
			model.set({updated : true});
			model.set({synced : false});
		})*/
		if($('#categorySelect').val() == 'ALL'){
			resetCasesCollection();
			return;
		}
		filterOnCategory($('#categorySelect').val());
	});
	$('#npeSelect').change(function(){
		/*var selectedModels = caseGrid.getSelectedModels();
		$.each(selectedModels, function(indesx, model){
			model.set('category', $('#categorySelect').val());
			model.set({updated : true});
			model.set({synced : false});
		})*/
		if($('#npeSelect').val() == 'ALL'){
			resetCasesCollection();
			return;
		}
		filterOnNPE($('#npeSelect').val());
	});	
	$('#logModal').on('hidden.bs.modal', function (e) {
		window.location.reload(true);
	});
});
function showCaseDetails(case_id, patentNumber) {
  currentCaseId = case_id;
	window.open("http://www.google.com/patents/US" + patentNumber, "_blank");
	getCaseDetail();
	getCaseHistory();
}
function getCaseDetail(){
  var DocumentCollection = Backbone.PageableCollection.extend({
    url : 'docs/' + currentCaseId,
    model : Case,
    mode : "client"
  });
  var docs = new DocumentCollection();
  var docGrid = new Backgrid.Grid({
    columns : documentGridColumns,
    collection : docs
  });
  var docPaginator = new Backgrid.Extension.Paginator({
    collection : docs
  });
  docs.fetch({
    reset : true,
    success: function(response){
      var documentStandAlone = [];
      var documentHeader = [];
      response.each(function(model, index){
        var doc = {
                "id": model.get("id"),
                "Name":model.get("name"),
                "Filling_Date":model.get("fillingDate"),
                "Paper_Number": model.get("paperNumber"),
                "Filling_Party": model.get("fillingParty"),
                "Availability":model.get("availability"),
                "Type":model.get("type")
        };
        if(model.get("type") == 'Petition' || model.get("type") == 'Reply' || model.get("type") == 'Preliminary Response'){
          documentHeader.push(doc);
        }else{
          documentStandAlone.push(doc);
        }
      });
      createCollapseTableData(documentStandAlone, documentHeader);
    }
  });
  
  function createCollapseTableData(standAloneDocs, headers){
    var docData = [];
    for(var i = 0; i < headers.length;i++){
      var doc = {
              "header":headers[i].Type,
              "Filling_Date":headers[i].Filling_Date,
              "exhibits":[],
              "document":headers[i]
      }
      var exhibits = [];
      for(var j = 0;j < standAloneDocs.length; j++){        
        if(standAloneDocs[j].Type == "Exhibit" && standAloneDocs[j].Filling_Date == doc.Filling_Date){
          exhibits.push(standAloneDocs[j]);
          standAloneDocs.splice(j,1);
        }
      }
      doc.exhibits = exhibits;
      docData.push(doc);
    }
    for(var i = 0;i<standAloneDocs.length;i++){
      docData.push(standAloneDocs[i]);
    }
    console.log(docData);
    createCollapsableTable(docData)
  }
  
  function createCollapsableTable(docData){
    var table = "<tr class='active'><th>id</th>" +
    		"<th>Name</th>" +
    		"<th>Type</th>" +
    		"<th>Filling Date</th>" +
    		"<th>Page Number</th>" +
    		"<th>Filling Party</th>" +
    		"<th>Availability</th></tr>";
    for(var i = 0 ;i<docData.length;i++){
      if(docData[i].header == undefined){
        table += createTableRow(docData[i], false, false);
      }else{
        table += "<tr class='active header inContainer' onclick='expandDocuments($(this))'><th></th>" +
        		"<th colspan='6'>"+docData[i].header+"<span>+</span></th></tr>";
        table += createTableRow(docData[i].document, true, false);
        for(var j = 0;j < docData[i].exhibits.length;j++){
          if(j == docData[i].exhibits.length -1){
            table += createTableRow(docData[i].exhibits[j], true, true);
          }else{
            table += createTableRow(docData[i].exhibits[j], true, false);
          }
        }
        table += "<tr class='break active'></tr>"
      }
    }
    $('#collapsableTable').html(table);
    $('tr.header').nextUntil('tr.break').slideUp(100, function() {
    });
  }
  
  function createTableRow(doc, isInContainer, isLast){
    var rowData = "";
    if(isInContainer == false && isLast == false){
      rowData += "<tr>";
    }else if(isInContainer == true && isLast == false){
      rowData += "<tr class='inContainer'>";
    }
    else if(isInContainer == true && isLast == true){
      rowData += "<tr class='isLast'>";
    }
    rowData += "<td>"+doc.id+"</td>" +
    "<td><a href='#' onclick=downloadFile(" + doc.id + ")>" + doc.Name + "</a></td>" +
    "<td>"+doc.Type+"</td>" +
    "<td>"+doc.Filling_Date+"</td>" +
    "<td>"+doc.Paper_Number+"</td>" +
    "<td>"+doc.Filling_Party+"</td>" +
    "<td>"+doc.Availability+"</td></tr>";
    return rowData;
  }
  /**
   * Commented as part of static case url refactoring. Req: 26
   */
  /*populateView(cases.findWhere({
  		id : currentCaseId
	}));*/
  //$("#caseMenu").removeClass("active");
  $("#docMenu").addClass("active");
  $("#docGrid").empty();
  $("#docPaginator").empty();
  $("#docGrid").append(docGrid.render().$el);
  $("#docPaginator").append(docPaginator.render().$el);
  $("#caseGridDiv").fadeOut(1000);
  $("#docGridDiv").fadeIn();
  $("#hiddenModelView").fadeIn();
  $("#docMenu").fadeIn();
}
function downloadFile(docId) {
	var $preparingFileModal = $("#preparing-file-modal");
	$preparingFileModal.dialog({
		modal : true
	});
	
	$.fileDownload(docDownloadURL+docId, {
		successCallback : function(url) {
			$preparingFileModal.dialog('close');
		},
		failCallback : function(responseHtml, url) {
			$preparingFileModal.dialog('close');
			$("#error-modal").dialog({
				modal : true
			});
		}
	});
	return false;
}

function populateView(caseObj) {
	$('#caseId').val(caseObj.get('caseId'));
	$('#applicationNumber').val(caseObj.get('applicationNumber'));
	if (caseObj.get('fillingDate') != null)
		$('#fillingDate').val($.format.date(caseObj.get('fillingDate'), 'yyyy-MM-dd'));
	$('#patentNumber').val(caseObj.get('patentNumber'));
	if (caseObj.get('decisionDate') != null)
		$('#decisionDate').val($.format.date(caseObj.get('decisionDate'), 'yyyy-MM-dd'));
	$('#petitioner').val(caseObj.get('petitioner'));
	$('#patentOwner').val(caseObj.get('patentOwner'));
	$('#status').val(caseObj.get('status'));
	$('#techCenter').val(caseObj.get('techCenter'));
	$('#category').val(caseObj.get('category'));
}

var rangeFilterType = undefined;
function showDateFilter(type) {
	if (rangeFilterType != type) {
		$("#hiddenRangeFilter").fadeOut(1000);
		$("#dateFrom").val("");
		$("#dateTo").val("");
	}
	rangeFilterType = type;
	$("#hiddenRangeFilter").fadeIn();
}

function resetRangeFilter() {
	resetCasesCollection();
	rangeFilterType = undefined;
	$("#dateFrom").val("");
	$("#dateTo").val("");
	$("#hiddenRangeFilter").fadeOut(1000);
	if($("#startDateFormGrp").hasClass('has-error') || $("#startDateFormGrp").hasClass('has-success')){
		$("#startDateFormGrp").removeClass('has-error');
		$("#startDateFormGrp").removeClass('has-success');
	}
	if($("#endDateFormGrp").hasClass('has-error') || $("#endDateFormGrp").hasClass('has-success')){
		$("#endDateFormGrp").removeClass('has-error');
		$("#endDateFormGrp").removeClass('has-success');
	}
}

function filterOnCategory(category){
	cases.trigger('doCategoryFilter', catgory);
}
function filterOnNPE(npe){
	cases.trigger('doNPEFilter', npe);
}
function filterOnFilingDate(fillingDate){
	cases.trigger('doFillingDateFilter', fillingDate);
}
function filterOnDecisionDate(decisionDate){
	cases.trigger('doDecisionDateFilter', decisionDate);
}
function filterOnTechCenter(techCenter){
	cases.trigger('doTechCenterFilter', techCenter);
}
function filterOnPetitioner(petitioner){
	cases.trigger('doPetitionerFilter', petitioner);
}
function filterOnPatentOwner(patentOwner){
	cases.trigger('doPatentOwnerFilter', patentOwner);
}
function filterOnStatus(status){
	cases.trigger('doStatusFilter', status);
}
function filterOnRange() {
	cases.trigger('doRangefilter', $("#dateFrom").val(), $("#dateTo").val(), rangeFilterType);
}
function filterCases(value, attrib){
	if(attrib == 'fillingDate')
		return filterOnFilingDate(value);
	if(attrib == 'decisionDate')
		return filterOnDecisionDate(value);
	if(attrib == 'techCenter')
		return filterOnTechCenter(value);
	if(attrib == 'petitioner')
		return filterOnPetitioner(value);
	if(attrib == 'patentOwner')
		return filterOnPatentOwner(value);
	if(attrib == 'status')
		return filterOnStatus(value);	
}
function fiterCasesWithInstutedStatus(){
	//$('#resetForQueryDiv').fadeIn(1000);
	cases.trigger('doInstitutedFilter');
}

function fiterCasesWithTerminatedStatus(){
	//$('#resetForQueryDiv').fadeIn(1000);
	cases.trigger('doTerminatedFilter');
}

function getCaseHistory(){
  var HistoryCollection = Backbone.PageableCollection.extend({
    url : 'history/'+currentCaseId,
    model : Case,
    mode : 'client'
  });
  var history = new HistoryCollection();
  var historyGrid = new Backgrid.Grid({
    columns : historyGridColumns,
    collection : history
  });
  var historyPaginator = new Backgrid.Extension.Paginator({
    collection : history
  });
  history.fetch({
    reset : true
  });
  $("#historyGrid").empty();
  $("#historyPaginator").empty();
  $("#historyGrid").append(historyGrid.render().$el);
  $("#historyPaginator").append(historyPaginator.render().$el);
}

var historyGridColumns = [{
  name : "id",
  cell : Backgrid.IntegerCell.extend({
    orderSeparator : ''
  }),
  sortable : false,
  editable : false
}, {
  name : "caseId",
  label : "Case Number",
  cell : "string",
  editable : false
}, {
  name : "applicationNumber",
  label : "Application Number",
  cell : "string",
  sortable : false,
  editable : false
}, {
  name : "fillingDate",
  label : "Filling date",
  cell : "date",
  editable : false
}, {
  name : "patentNumber",
  label : "Patent Number",
  cell : "string",
  editable : false
}, {
  name : "decisionDate",
  label : "Decision Date",
  cell : "date",
  sortable : false,
  editable : false
}, {
  name : "petitioner",
  label : "Petitioner",
  cell : "text",
  editable : false
}, {
  name : "patentOwner",
  label : "Patent Owner",
  cell : "text",
  editable : false
}, {
  name : "status",
  label : "Status",
  cell : "string",
  editable : false
}, {
  name : "techCenter",
  label : "Tech Center",
  cell : "string",
  editable : false
} ];


var json = [];
function createStatusChartJson(data) {
  var parsed = false;
  for (var i = 0; i < json.length; i++) {
    if (json[i].status == data) {
      json[i].number = json[i].number + 1;
      parsed = true;
    }
  }
  if (!parsed) {
    json.push({
      "status": data,
      "number": 1
    });
  }
}

function createTechCenterChartJson(data) {
  var parsed = false;
  if(data == "****"){
    data = "private";
  }
  for (var i = 0; i < json.length; i++) {
    if (json[i].techCenter == data) {
      json[i].number = json[i].number + 1;
      parsed = true;
    }
  }
  if (!parsed) {
    json.push({
      "techCenter": data,
      "number": 1
    });
  }
}

function createChart(titleField) {
  var chart = AmCharts.makeChart("chartDiv", {
    "type": "pie",
    "theme": "light",
    "outlineAlpha":0.8,
    "outlineThickness":2,
    "legend": {
      "markerType": "circle",
      "position": "right",
      "marginRight": 80,
      "autoMargins": false
    },
    "dataProvider":json,
  "valueField": "number",
  "titleField": titleField,
    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>"
  });
}

function showChart(against) {
  json.splice(0,json.length);
  var chartCollection = Backbone.Collection.extend({
    model: Case,
    url: 'rest/patents/cases',
    mode: "client"
  })
  var models = new chartCollection({});
  $("#caseMenu").removeClass("active");
  $('#chartView').fadeIn();
  $('#hiddenModelView').fadeOut(1000);
  $('#docGridDiv').fadeOut(1000);
  $('#caseGridDiv').fadeOut(1000);
  if(against == 'status'){
    $('#chartHeading').html('Status Chart');
  }else if(against == 'techCenter'){
    $('#chartHeading').html('TechCenter Chart');
  }
  models.fetch({
    reset: true,
    success: function(response) {
      response.each(function(model, index) {
        if (against == 'status') {
          createStatusChartJson(model.get("status"));
        } else if (against == 'techCenter') {
          createTechCenterChartJson(model.get("techCenter"));
        }
      });
      createChart(against);
    }
  });
}