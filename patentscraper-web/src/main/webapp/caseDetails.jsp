<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<title>Unified Patents - PTAB</title>
<meta charset="utf-8">

<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.css">
<link rel="stylesheet" href='<c:url value="/resources/flat-ui/css/flat-ui.css" />'>
<link rel="stylesheet" href='<c:url value="/resources/flat-ui/css/bootstrap-switch.css" />'>
<link rel="stylesheet"
	href='<c:url value="/resources/backgrid/css/backgrid-paginator.css" />'>
<link rel="stylesheet" href='<c:url value="/resources/backgrid/css/backgrid.css"/>'>
<link rel="stylesheet" href='<c:url value="/resources/backgrid/css/backgrid-text-cell.css" />'>
<link rel="stylesheet" href='<c:url value="/resources/backgrid/css/backgrid-filter.css" />'>
<link rel="stylesheet" href='<c:url value="/resources/backgrid/css/backgrid-select-all.css" />'>
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href='<c:url value="/resources/css/application.css" />'>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script
	src="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src='<c:url value="/resources/backgrid/js/bootstrap-modal.js" />'></script>
<script src='<c:url value="/resources/flat-ui/js/flatui-checkbox.js" />'></script>
<script src='<c:url value="/resources/flat-ui/js/bootstrap-switch.js" />'></script>
<script src='<c:url value="/resources/flat-ui/js/flatui-radio.js" />'></script>
<script src='<c:url value="/resources/flat-ui/js/bootstrap-select.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/underscore-1.6.0.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backbone1.1.2.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backbone.paginator.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid-paginator.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid-text-cell.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid-select-all.js" />'></script>
<script src='<c:url value="/resources/backgrid/js/backgrid-filter.js" />'></script>
<script src='<c:url value="/resources/plugins/jquery-file-download/js/jquery.filedownload.js" />'></script>
<script src='<c:url value="/resources/plugins/jquery.dateFormat/js/dateFormat.js" />'></script>
<script src='<c:url value="/resources/plugins/jquery.dateFormat/js/jquery.dateFormat.js" />'></script>
<script src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="http://www.amcharts.com/lib/3/pie.js"></script>
<script src="http://www.amcharts.com/lib/3/themes/light.js"></script>
<script type="text/javascript">
var docDownloadURL = '<c:url value="/rest/patents/download/docs/"/>';
	$(document).ready(function(){
		currentCaseId = "<c:out value='${caseObj.id}'/>";
		getCaseDetail();
		getCaseHistory();
		$('#homeMenu').click(function(){
			window.location.href = '<c:url value="/index.jsp"/>';
		});
		$('#brandTab').click(function(){
			window.location.href = '<c:url value="/index.jsp"/>';
		});		
	});
	var searchableCell = Backgrid.Cell.extend({
		render : function() {
			this.$el.html('<a href="#" onclick="filterCases(\'' + this.model.get(this.column.get('name')) + '\',\'' + this.column.get('name') + '\')">' + this.model.get(this.column.get('name')) + '</a>');
			this.delegateEvents();
			return this;
		}
	});	
	var searchableDateCell = Backgrid.DateCell.extend({
		render : function() {
			this.$el.html('<a href="#" onclick="filterCases(\'' + this.formatter.fromRaw(this.model.get(this.column.get("name")), this.model) + '\',\'' + this.column.get('name') + '\')">' + this.formatter.fromRaw(this.model.get(this.column.get("name")), this.model) + '</a>');
			this.delegateEvents();
			return this;
		}
	});	
	var docNameCell = Backgrid.Cell.extend({
		render : function() {
			this.$el.html('<a href="#" onclick=downloadFile(' + this.model.get("id") + ')>' + this.model.get('name') + '</a>');
			this.delegateEvents();
			return this;
		}
	});
	var caseNumCell = Backgrid.Cell.extend({
		render : function() {
			this.$el.html('<a href="#" onclick=openCaseDetails(\'' + this.model.get('caseId') + '\',\'' + this.model.get('patentNumber') + '\')>' + this.model.get('caseId') + '</a>');
			this.delegateEvents();
			return this;
		}
	});
	// Below cell editor is un-used yet an importan working example
	var CategorySelectCellEditor = Backgrid.SelectCellEditor.extend({
		save : function() {
			var model = this.model;
			var column = this.column;
			model.set(column.get("name"), this.formatter.toRaw(this.$el.val(), model));
			alert('I am called!!');
		}
	});
	
	
	  function expandDocuments(el) {
	    el.find('span').text(function(_, value) {
	      return value == '-' ? '+' : '-'
	    });
	    el.nextUntil('tr.break').slideToggle(100, function() {
	    });
	  }
</script>
<script src='<c:url value="/resources/js/caseColumns.js" />'></script>
<script src='<c:url value="/resources/js/custom.js" />'></script>
</head>
<jsp:useBean id="caseObj" class="org.unifiedpatents.model.Case" scope="request" />
<body>
	<div class="container" style="overflow: hidden; width: auto;">
		<jsp:include page="jsps/header.jsp"/>
		<jsp:include page="jsps/top-nav-without-filters.jsp"/>
		<div id="preparing-file-modal" title="Preparing report..."
			style="display: none;">
			Your download is being prepared. Please wait...
	
			<div class="ui-progressbar-value ui-corner-left ui-corner-right"
				style="width: 100%; height: 22px; margin-top: 20px;"></div>
		</div>

		<div id="error-modal" title="Error" style="display: none;">There
			was a problem downloading the document, please try again.
		</div>		  
		<div class="row" id="containerDiv">
			<div class="col-md-12" id="main-pane">
				<div id="hiddenModelView" style="display: none;">
					<form action="#" class="form-horizontal" role="form">
						<div class="form-group">
							<label for="caseId" class="col-md-2 control-label">Case
								Number</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="caseId" disabled value='<c:out value="${caseObj.caseId}"/>'>
							</div>
				
							<label for="fillingDate" class="col-md-2 control-label">Filling
								Date</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="fillingDate" disabled value='<c:out value="${caseObj.fillingDate}"/>'>
							</div>
						</div>
						<div class="form-group">
							<label for="decisionDate" class="col-md-2 control-label">Decision
								Date</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="decisionDate" disabled value='<c:out value="${caseObj.decisionDate}"/>'>
							</div>
				
							<label for="patentNumber" class="col-md-2 control-label">Patent
								Number</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="patentNumber" disabled value='<c:out value="${caseObj.patentNumber}"/>'>
							</div>
						</div>
						<div class="form-group">
							<label for="applicationNumber" class="col-md-2 control-label">Application
								Number</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="applicationNumber" disabled value='<c:out value="${caseObj.applicationNumber}"/>'>
							</div>
				
							<label for="status" class="col-md-2 control-label">Status</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="status" disabled value='<c:out value="${caseObj.status}"/>'>
							</div>
						</div>
						<div class="form-group">
							<label for="techCenter" class="col-md-2 control-label">Tech
								Center</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="techCenter" disabled value='<c:out value="${caseObj.techCenter}"/>'>
							</div>
				
							<label for="category" class="col-md-2 control-label">Category</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="category" disabled value='<c:out value="${caseObj.category}"/>'>
							</div>
						</div>
						<div class="form-group">
							<label for="petitioner" class="col-md-2 control-label">Petitioner</label>
							<div class="col-md-4">
								<!-- <textarea rows="5" cols="50" class="form-control" id="petitioner" disabled></textarea> -->
								<input type="text" class="form-control" id="petitioner" disabled value='<c:out value="${caseObj.petitioner}"/>'>
							</div>
				
							<label for="patentOwner" class="col-md-2 control-label">Patent
								Owner</label>
							<div class="col-md-4">
								<!-- <textarea rows="5" cols="50" class="form-control" id="patentOwner" disabled></textarea> -->
								<input type="text" class="form-control" id="patentOwner" disabled value='<c:out value="${caseObj.patentOwner}"/>'>
							</div>
						</div>
						<div class="form-group">
							<label for="techCenter" class="col-md-2 control-label">NPE</label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="techCenter" disabled value='<c:out value="${caseObj.npe}"/>'>
							</div>
						</div>		
					</form>
				</div>
				<div id="docGridDiv" style="display: none;">
				<ul class="nav nav-tabs" role="tablist" id="myTab">
				  <li class="active" id="document-tab"><a href="#tab-document" role="tab" data-toggle="tab">Document</a></li>
				  <li id="history-tab"><a href="#tab-history" role="tab" data-toggle="tab">History</a></li>
				</ul>
				<div class="tab-content">
				  <div class="tab-pane active" id="tab-document">
				  	<!-- <div id="docGrid"></div>
					<div id="docPaginator"></div> -->
					<table class="table table-condensed" id="collapsableTable">
					</table>
				  </div>
				  <div class="tab-pane" id="tab-history">
				  	<div id="historyGrid"></div>
					<div id="historyPaginator"></div>
				  </div>
				</div>	
				</div>				
			</div>
		</div>
	</div>
</body>