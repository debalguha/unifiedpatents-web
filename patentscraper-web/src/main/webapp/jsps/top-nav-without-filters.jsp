<nav class="navbar navbar-inverse navbar-embossed" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target="#navbar-collapse-01">
			<span class="sr-only">Toggle navigation</span>
		</button>
		<a id="brandTab" class="navbar-brand" href="#">PTAB</a>
	</div>
	<div class="collapse navbar-collapse" id="navbar-collapse-01">
		<ul class="nav navbar-nav">
			<li id="homeMenu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-home"></i>Home</a>
			</li>
			<li id="docMenu" class="active"><a href="#fakelink">Details</a></li>
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>