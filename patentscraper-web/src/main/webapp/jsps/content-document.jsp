<div id="hiddenModelView" style="display: none;">
	<form action="#" class="form-horizontal" role="form">
		<div class="form-group">
			<label for="caseId" class="col-md-2 control-label">Case
				Number</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="caseId" disabled value='<jsp:getProperty property="caseId" name="case"/>'>
			</div>

			<label for="fillingDate" class="col-md-2 control-label">Filling
				Date</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="fillingDate" disabled value='<jsp:getProperty property="fillingDate" name="case"/>'>
			</div>
		</div>
		<div class="form-group">
			<label for="decisionDate" class="col-md-2 control-label">Decision
				Date</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="decisionDate" disabled value='<jsp:getProperty property="decisionDate" name="case"/>'>
			</div>

			<label for="patentNumber" class="col-md-2 control-label">Patent
				Number</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="patentNumber" disabled value='<jsp:getProperty property="patentNumber" name="case"/>'>
			</div>
		</div>
		<div class="form-group">
			<label for="applicationNumber" class="col-md-2 control-label">Application
				Number</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="applicationNumber" disabled value='<jsp:getProperty property="applicationNumber" name="case"/>'>
			</div>

			<label for="status" class="col-md-2 control-label">Status</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="status" disabled value='<jsp:getProperty property="status" name="case"/>'>
			</div>
		</div>
		<div class="form-group">
			<label for="techCenter" class="col-md-2 control-label">Tech
				Center</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="techCenter" disabled value='<jsp:getProperty property="techCenter" name="case"/>'>
			</div>

			<label for="category" class="col-md-2 control-label">Category</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="category" disabled value='<jsp:getProperty property="category" name="case"/>'>
			</div>
		</div>
		<div class="form-group">
			<label for="petitioner" class="col-md-2 control-label">Petitioner</label>
			<div class="col-md-4">
				<!-- <textarea rows="5" cols="50" class="form-control" id="petitioner" disabled></textarea> -->
				<input type="text" class="form-control" id="petitioner" disabled value='<jsp:getProperty property="petitioner" name="case"/>'>
			</div>

			<label for="patentOwner" class="col-md-2 control-label">Patent
				Owner</label>
			<div class="col-md-4">
				<!-- <textarea rows="5" cols="50" class="form-control" id="patentOwner" disabled></textarea> -->
				<input type="text" class="form-control" id="patentOwner" disabled value='<jsp:getProperty property="patentOwner" name="case"/>'>
			</div>
		</div>
		<div class="form-group">
			<label for="techCenter" class="col-md-2 control-label">NPE</label>
			<div class="col-md-2">
				<input type="text" class="form-control" id="techCenter" disabled value='<jsp:getProperty property="npe" name="case"/>'>
			</div>
		</div>		
	</form>
</div>
<div id="docGridDiv" style="display: none;">
<ul class="nav nav-tabs" role="tablist" id="myTab">
  <li class="active" id="document-tab"><a href="#tab-document" role="tab" data-toggle="tab">Document</a></li>
  <li id="history-tab"><a href="#tab-history" role="tab" data-toggle="tab">History</a></li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="tab-document">
  	<div id="docGrid"></div>
	<div id="docPaginator"></div>
  </div>
  <div class="tab-pane" id="tab-history">
  	<div id="historyGrid"></div>
	<div id="historyPaginator"></div>
  </div>
</div>	
</div>