<div id="hiddenRangeFilter" style="display: none;">
		<form action="#" class="form-inline" role="form">
		  <div class="form-group" id="startDateFormGrp">
		  	<div class="input-group">
		    	<div class="input-group-addon">
		    		<span class="glyphicon glyphicon-calendar"></span>
		    	</div>
		      	<input type="text" class="form-control date-picker" id="dateFrom" placeholder="From"/>
		    </div>
		    </div>
		   <div class="form-group" id="endDateFormGrp">
		  	<div class="input-group">
		    	<div class="input-group-addon">
		    		<span class="glyphicon glyphicon-calendar"></span>
		    	</div>
		      	<input type="text" class="form-control date-picker" id="dateTo" placeholder="To">
		    </div>
		    </div>
    	<button class="btn btn-primary" id="rangeFilter">Filter</button>
    	<button class="btn" id="clearFilter">Clear</button>
		  
		</form>
	</div>
        
