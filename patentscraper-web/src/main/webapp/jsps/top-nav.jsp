<nav class="navbar navbar-inverse navbar-embossed" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target="#navbar-collapse-01">
			<span class="sr-only">Toggle navigation</span>
		</button>
		<a id="brandTab" class="navbar-brand" href="#">PTAB</a>
	</div>
	<div class="collapse navbar-collapse" id="navbar-collapse-01">
		<ul class="nav navbar-nav">
			<li id="caseMenu" class="active dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Cases</a>
				<span class="dropdown-arrow"></span>
				<ul class="dropdown-menu">
				  <li><a id="sync-action" href="#">Sync Cases</a></li>
				  <li><a id="reset-action" href="#">Reset Cases</a></li>
				</ul>
			</li>
			<li id="docMenu" style="display: none;"><a href="#fakelink">Details</a></li>
		</ul>
		<div style="float: right !important;">
			<table>
				<tr>
					<td><form class="navbar-form navbar-right" action="#"
							role="search" style="width: 230px;">
							<div id="petitionerFilterWrapper">
							</div>
						</form></td>
					<td><form class="navbar-form navbar-right" action="#"
							role="search" style="width: 230px;">
							<div id="patentOwnerFilterWrapper">
							</div>
						</form></td>
					<td><form class="navbar-form navbar-right" action="#"
							role="search" style="width: 230px;">
							<div id="caseNameFilterWrapper">
							</div>
						</form></td>
					<td><form class="navbar-form navbar-right" action="#"
							role="search" style="width: 230px;">
							<div id="applicationNumbereFilterWrapper" class="form-group">
							</div>
						</form></td>
					<td><form class="navbar-form navbar-right" action="#"
							role="search" style="width: 230px;">
							<div id="patentNumberFilterWrapper" class="form-group">
							</div>
						</form>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- /.navbar-collapse -->
</nav>