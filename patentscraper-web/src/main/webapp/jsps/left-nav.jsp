<div class="panel-group todo" id="accordion">
	<div class="panel panel-default">
		<div class="panel-heading todo-search" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="cursor: pointer;">
			<h4 class="panel-title">
        		<a>
					<span class="todo-search-field" style="font-weight: bold;">Range Filters</span>				
				</a>
      		</h4>		
		</div>
	    <div id="collapseOne" class="panel-collapse collapse in">
	    	<div class="panel-body" style="padding: 0px;">
				<ul>
					<li class="" onclick="showDateFilter('onDecision')">
						<!-- <span class="glyphicon glyphicon-filter" style="float:left; padding: 11px 22px 0 0;"></span> -->
						<div class="todo-icon fui-filter"></div>
						<div class="todo-content">
							<h4 class="todo-name">
								Decision Date
							</h4>
						</div>
					</li>
					<li class="" onclick="showDateFilter('onFilling')">
						<div class="todo-icon fui-filter"></div>
						<div class="todo-content">
							<h4 class="todo-name">
								Filing Date
							</h4>
						</div>
					</li>
				</ul>	    	
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading todo-queries" data-toggle="collapse" data-parent="#accordion" href="#queries" style="cursor: pointer;">
			<h4 class="panel-title">
        		<a data-toggle="collapse" data-parent="#accordion" href="#queries">
					<span class="todo-search-field" style="font-weight: bold;">Queries</span>		
				</a>
      		</h4>		
		</div>
	    <div id="queries" class="panel-collapse collapse">
	    	<div class="panel-body" style="padding: 0px;">
				<ul>
					<li class="" onclick="fiterCasesWithInstutedStatus()">
						<!-- <span class="glyphicon glyphicon-filter" style="float:left; padding: 11px 22px 0 0;"></span> -->
						<div class="todo-icon fui-run-query"></div>
						<div class="todo-content">
							<h4 class="todo-name">
								Instituted IPRs
							</h4>
						</div>
					</li>
					<li class="" onclick="fiterCasesWithTerminatedStatus()">
						<div class="todo-icon fui-run-query"></div>
						<div class="todo-content">
							<h4 class="todo-name">
								Terminated IPRs
							</h4>
						</div>
					</li>
				</ul>	    	
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading todo-analytics">
			<h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#analytics" style="cursor: pointer;">
        		<a data-toggle="collapse" data-parent="#accordion" href="#analysis">
					<span class="todo-search-field" style="font-weight: bold;">Analytics</span>
				</a>
      		</h4>		
		</div>
	    <div id="analytics" class="panel-collapse collapse">
	    	<div class="panel-body" style="padding: 0px;">
				<ul>
					<li class="" onclick="showChart('status')">
						<!-- <span class="glyphicon glyphicon-filter" style="float:left; padding: 11px 22px 0 0;"></span> -->
						<div class="todo-icon fui-pie-chart"></div>
						<div class="todo-content">
							<h4 class="todo-name">
								Status Chart
							</h4>
						</div>
					</li>
					<li class="" onclick="showChart('techCenter')">
						<div class="todo-icon fui-bar-chart"></div>
						<div class="todo-content">
							<h4 class="todo-name">
								Techcenter chart
							</h4>
						</div>
					</li>
				</ul>	    	
			</div>
		</div>
	</div>		
</div>