<div id="caseGridDiv"style="font-size: 10px;">
  	<div class="row" id="containerDiv" style="margin-bottom: 15px;">
		<div class="col-md-7" style="display: table-column;">
			<table><tr>
				<td style="padding: 2px;">
					<button class="btn btn-primary" id="settings" data-toggle="modal" data-target=".bs-example-modal-sm" title="Adjust grid">Grid Settings<span class="fui-gear" style="float: right; margin-left: 5px;"></span></button>
				</td>
				<td style="padding: 2px;">
					<label for="categorySelect">Zone: </label>
					<select id="categorySelect">
						<option value="ALL" selected="selected">All</option>
						<option value="CLOUD">Cloud</option>
						<option value="CONTENT">Content</option>
						<option value="COMMERCE">Commerce</option>
						<option value="NA">None</option>
					</select>				
				</td>
				<td style="padding: 2px;">
					<label for="npeSelect">NPE: </label>
					<select id="npeSelect">
						<option value="ALL" selected="selected">All</option>
						<option value="YES">Yes</option>
						<option value="NO">No</option>
					</select>				
				</td>
				<!-- <td>
					<input type="checkbox" checked data-toggle="switch" />
				</td>	 -->		
				</tr></table>
			<!-- <a id="settings" data-toggle="modal" data-target=".bs-example-modal-sm" title="Adjust grid"><span class="fui-gear" style="float: right; height: 30px; width: 17px;"></span></a> -->

		</div>
  	</div>
	<div class="row">
		<div class="col-md-12">
		    <div id="caseGrid"></div>
		    <div id="casePaginator"></div>
    	</div>
    </div>	
</div>