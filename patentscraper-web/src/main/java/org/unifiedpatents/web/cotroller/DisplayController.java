package org.unifiedpatents.web.cotroller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/display")
public class DisplayController {
	private static final Log logger = LogFactory.getLog(DisplayController.class);
	
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public ModelAndView doDisplay(HttpServletRequest request){
		Map<String, String[]> map = request.getParameterMap();
		for(String key : map.keySet())
			logger.info(key+":"+StringUtils.arrayToCommaDelimitedString(map.get(key)));
		return new ModelAndView("index");
	}
}
