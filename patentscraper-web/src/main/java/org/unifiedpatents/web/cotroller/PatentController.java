package org.unifiedpatents.web.cotroller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.unifiedpatents.model.Case;
import org.unifiedpatents.model.CaseDocument;
import org.unifiedpatents.model.CaseHistory;
import org.unifiedpatents.model.Category;
import org.unifiedpatents.model.DocumentURL;
import org.unifiedpatents.model.NPE;
import org.unifiedpatents.persistence.CaseDao;
import org.unifiedpatents.scraper.worker.impl.DocumentURLGetWorker;
import org.unifiedpatents.scraper.worker.impl.PatentSearchSubmitWorker;

@Controller
@RequestMapping("/patents")
public class PatentController {
	private static final Log logger = LogFactory.getLog(PatentController.class);
	@Autowired
	private CaseDao caseDao;
	
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static final DateFormat dateFormatDrillDown = new SimpleDateFormat("yyyy-MM-dd");

	public void setCaseDao(CaseDao caseDao) {
		this.caseDao = caseDao;
	}
	
	@RequestMapping(value = "/cases", method = RequestMethod.GET)
	public @ResponseBody List<Case> fetchCases(@RequestParam(value="from", required=false) String from,
			@RequestParam(value="to", required=false) String to,
			@RequestParam(value="type", required=false) String type) throws ParseException{
		if(from!=null && to!=null && type!=null){
			Date dateFrom = dateFormat.parse(from);
			Date dateTo = dateFormat.parse(to);
			if(type.equals("onDecision"))
				return caseDao.findAllCasesFilterDecisionDate(dateFrom, dateTo);
			else
				return caseDao.findAllCasesFilterFillingDate(dateFrom, dateTo);
		}else
			return caseDao.findAllCases();
	}
	
	@RequestMapping(value = "/cases/{caseId}", method = RequestMethod.GET, produces="text/html")
	public ModelAndView fetchCase(@PathVariable(value="caseId") String caseId) throws ParseException{
		logger.info("Case Id: "+caseId);
		Case caseObj = caseDao.findByCaseId(caseId);
		return new ModelAndView("caseDetails", "caseObj", caseObj);
	}
	
	@RequestMapping(value = "/docs/{case_id}", method = RequestMethod.GET)
	public @ResponseBody Set<CaseDocument> fetchDocs(@PathVariable(value="case_id") long case_id){
		return caseDao.findAllDocumentsForCase(case_id);
	}	
	
	@RequestMapping(value = "/cases/docs/{case_id}", method = RequestMethod.GET)
	public @ResponseBody Set<CaseDocument> fetchDocsNew(@PathVariable(value="case_id") long case_id){
		return fetchDocs(case_id);
	}	
	
	@RequestMapping(value = "/download/docs/{doc_id}", method = RequestMethod.GET)
	public void fetchDocsRaw(@PathVariable(value="doc_id") long doc_id, HttpServletResponse response) throws Exception{
		DocumentURL docUrl = caseDao.findDocumentURLForDocumentId(doc_id);
		PatentSearchSubmitWorker worker = new PatentSearchSubmitWorker("https://ptabtrials.uspto.gov/prweb/PRWebLDAP2/HcI5xOSeX_yQRYZAnTXXCg%5B%5B*/!STANDARD?UserIdentifier=searchuser", false, null);
		Object[] rets = worker.call();
		DocumentURLGetWorker documentWorker = new DocumentURLGetWorker(false, null, new HttpGet(docUrl.getUrl()), (CookieStore)rets[1], caseDao, docUrl);
		Object[] fileContent = documentWorker.doWorkAndDownloadFile();
		response.setHeader("Set-Cookie", "fileDownload=true; path=/");
		response.setHeader("Cache-Control", "must-revalidate");
		response.setHeader("Content-Type", "text/csv");
		response.setHeader("Content-disposition", fileContent[0].toString());
		logger.info("Content-Disposition: "+fileContent[0].toString());
		logger.info("Byte Array: "+fileContent[1]);
		IOUtils.copy(new ByteArrayInputStream((byte[])fileContent[1]), response.getOutputStream());
	}
	
	@RequestMapping(value = "/case/update", method = RequestMethod.POST)
	public @ResponseBody String updateCase(HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));
		Category category = !StringUtils.isEmpty(request.getParameter("category"))?Category.valueOf(request.getParameter("category")):null;
		String litigationLink = request.getParameter("litigationLink");
		try {
			caseDao.updateCaseWithNewCategoryAndNpeLink(id, category, litigationLink);
		} catch (Exception e) {
			logger.error("Unable to update category for id:"+id, e);
			return "FAILED";
		}
		return "SUCCESS";
	}
	
	@RequestMapping(value = "/case/update/new", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody String updateCase_New(@RequestBody Case caseObj){
		try {
			if(!caseDao.updateCase(caseObj))
				return "UNCHANGED";
		} catch (Exception e) {
			logger.error("Unable to update case :"+caseObj, e);
			return "FAILED";
		}
		return "SUCCESS";
	}	
	
	@RequestMapping(value = "/cases/report/instituted", method = RequestMethod.GET)
	public @ResponseBody List<Case> findInstitutedIPRsInLast30Days(){
		return caseDao.findInstitutedIPRsInLast30Days();
	}
	
	@RequestMapping(value = "/cases/report/terminated", method = RequestMethod.GET)
	public @ResponseBody List<Case> findTerminatedIPRsWithoutDecisionInLast30Days(){
		Calendar startCal = Calendar.getInstance();
		startCal.add(Calendar.DAY_OF_YEAR, -30);
		Calendar endCal = Calendar.getInstance();
		return caseDao.findCasesTerminatedWithoutDecision(startCal.getTime(), endCal.getTime());
	}
	
	@RequestMapping(value = "/cases/category/{category}", method = RequestMethod.GET)
	public @ResponseBody List<Case> findCasesWithCategorys(@PathVariable(value="category") Category category){
		return caseDao.findCasesWithCategory(category);
	}
	
	@RequestMapping(value = "/cases/npe/{npe}", method = RequestMethod.GET)
	public @ResponseBody List<Case> findCasesWithNPE(@PathVariable(value="npe") NPE npe){
		return caseDao.findCasesWithNPE(npe);
	}
	
	@RequestMapping(value = "/cases/techCenter/{techCenter}", method = RequestMethod.GET)
	public @ResponseBody List<Case> findCasesWithTechCenter(@PathVariable(value="techCenter") String techCenter){
		return caseDao.findCasesWithTechCenter(techCenter);
	}
	
	@RequestMapping(value = "/cases/status/{status}", method = RequestMethod.GET)
	public @ResponseBody List<Case> findCasesWithStatus(@PathVariable(value="status") String status){
		return caseDao.findCasesWithStatus(status);
	}	
	
	@RequestMapping(value = "/cases/petitioner/{petitioner}/", method = RequestMethod.GET)
	public @ResponseBody List<Case> findCasesWithPetitioners(@PathVariable(value="petitioner") String petitioner){
		return caseDao.findCasesWithPetitioners(petitioner);
	}
	
	@RequestMapping(value = "/cases/patentOwner/{patentOwner}/", method = RequestMethod.GET)
	public @ResponseBody List<Case> findCasesWithPatentOwner(@PathVariable(value="patentOwner") String patentOwner){
		logger.info("Patent Owner: "+patentOwner);
		return caseDao.findCasesWithPatentOwner(patentOwner);
	}	
	
	@RequestMapping(value = "/cases/decisionDate/{decisionDate}", method = RequestMethod.GET)
	public @ResponseBody List<Case> findCasesWithDecisionDate(@PathVariable(value="decisionDate") String decisionDate) throws ParseException{
		Calendar dateLow = Calendar.getInstance();
		dateLow.setTime(dateFormatDrillDown.parse(decisionDate));
		dateLow.set(Calendar.HOUR_OF_DAY, 0);
		dateLow.set(Calendar.MINUTE, 0);
		dateLow.set(Calendar.SECOND, 0);
		
		Calendar dateHigh = Calendar.getInstance();
		dateHigh.setTime(dateFormatDrillDown.parse(decisionDate));
		dateHigh.set(Calendar.HOUR_OF_DAY, 23);
		dateHigh.set(Calendar.MINUTE, 59);
		dateHigh.set(Calendar.SECOND, 59);
		return caseDao.findAllCasesFilterDecisionDate(dateLow.getTime(), dateHigh.getTime());
	}
	
	@RequestMapping(value = "/cases/fillingDate/{fillingDate}", method = RequestMethod.GET)
	public @ResponseBody List<Case> findCasesWithFillingDate(@PathVariable(value="fillingDate") String fillingDate) throws ParseException{
		Calendar dateLow = Calendar.getInstance();
		dateLow.setTime(dateFormatDrillDown.parse(fillingDate));
		dateLow.set(Calendar.HOUR_OF_DAY, 0);
		dateLow.set(Calendar.MINUTE, 0);
		dateLow.set(Calendar.SECOND, 0);
		
		Calendar dateHigh = Calendar.getInstance();
		dateHigh.setTime(dateFormatDrillDown.parse(fillingDate));
		dateHigh.set(Calendar.HOUR_OF_DAY, 23);
		dateHigh.set(Calendar.MINUTE, 59);
		dateHigh.set(Calendar.SECOND, 59);
		return caseDao.findAllCasesFilterFillingDate(dateLow.getTime(), dateHigh.getTime());
	}
	
	
	
	@RequestMapping(value = "/history/{case_id}", method = RequestMethod.GET)
	public @ResponseBody List<CaseHistory> fetchCaseHistory(@PathVariable(value="case_id") long case_id){
		return caseDao.findCaseHistoryByCaseId(case_id);
	}
	
	@RequestMapping(value = "/cases/history/{case_id}", method = RequestMethod.GET)
	public @ResponseBody List<CaseHistory> fetchCaseHistoryNew(@PathVariable(value="case_id") long case_id){
		return fetchCaseHistory(case_id);
	}
	
	@RequestMapping(value = "/settings", method = RequestMethod.POST)
	public void doDisplay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Map<String, String[]> map = request.getParameterMap();
		for(String key : map.keySet())
			logger.info(key+":"+StringUtils.arrayToCommaDelimitedString(map.get(key)));
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}
	
	@ExceptionHandler(Throwable.class)
	public @ResponseBody String handleException(Throwable t){
		logger.error("Exception: ", t);
		return "ERROR";
	}
	
}