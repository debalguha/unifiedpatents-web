package org.unifiedpatents.web.cotroller;

import java.util.List;

import org.unifiedpatents.model.BaseEntity;

public class PatentResponse {
	private int totalRecords;
	private List<?> items;
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
	public List<?> getItems() {
		return items;
	}
	public void setItems(List<BaseEntity> items) {
		this.items = items;
	}
	public PatentResponse(int totalRecords, List<?> items) {
		super();
		this.totalRecords = totalRecords;
		this.items = items;
	}
	
}
