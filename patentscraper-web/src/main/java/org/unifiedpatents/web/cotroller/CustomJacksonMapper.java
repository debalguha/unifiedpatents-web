package org.unifiedpatents.web.cotroller;

import java.io.IOException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;

@SuppressWarnings("serial")
public class CustomJacksonMapper extends ObjectMapper {

	public CustomJacksonMapper() {
	    super();
	    this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    this.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,true);
	    this.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	    this.getSerializerProvider().setNullValueSerializer(new NullSerializer());
	    this.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
	}
}

class NullSerializer extends JsonSerializer<Object> {
	public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeString("");
	}
}