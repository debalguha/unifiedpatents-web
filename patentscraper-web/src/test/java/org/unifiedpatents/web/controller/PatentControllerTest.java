package org.unifiedpatents.web.controller;

import org.junit.Test;
import org.unifiedpatents.web.cotroller.PatentController;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PatentControllerTest extends BaseTestCase{
	
	@Test
	public void shouldBeAbleToRetrieveOnlyCasesOmmitingDocs() throws Exception{
		PatentController controller = childCtx.getBean(PatentController.class);
		new ObjectMapper().writeValue(System.out, controller.fetchCases(null, null, null));
	}
}
